#!/bin/bash

HOST=`/bin/hostname`
#SERVER='192.168.10.14'
ZABBIX_SERVER="$1"
CURL=/usr/bin/curl

read -a stat <<< `$CURL -sm3 --insecure "https://127.0.0.1/nginx-stats"`

[ ! -z $stat ] && {
    echo "\
${HOST} nginx.active ${stat[2]}
${HOST} nginx.accepts ${stat[7]}
${HOST} nginx.handled ${stat[8]}
${HOST} nginx.requests ${stat[9]}
${HOST} nginx.reading ${stat[11]}
${HOST} nginx.writing ${stat[13]}
${HOST} nginx.waiting ${stat[15]}" |
    /usr/bin/zabbix_sender \
	--zabbix-server ${ZABBIX_SERVER} -s ${HOST} \
	--port '10051' -i - >/dev/null 2>&1

}


echo "1"
exit 0
